// Fill out your copyright notice in the Description page of Project Settings.


#include "Deck.h"
#include <random>

// Sets default values
ADeck::ADeck()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>("BaseMesh");
	SetRootComponent(BaseMesh);
		
}

// Called when the game starts or when spawned
void ADeck::BeginPlay()
{
	Super::BeginPlay();

	//fill all cards default
	CollectOrderedDeck();
	ShuffleDeckOfCards();
}

void ADeck::CollectOrderedDeck()
{
	//Clubs
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::RankA));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank2));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank3));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank4));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank5));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank6));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank7));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank8));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank9));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::Rank10));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::RankJ));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::RankQ));
	OrderedDeck.Add(CreatNewCard(ESuit::Clubs, ERank::RankK));

	//Diamonds
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::RankA));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank2));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank3));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank4));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank5));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank6));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank7));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank8));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank9));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::Rank10));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::RankJ));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::RankQ));
	OrderedDeck.Add(CreatNewCard(ESuit::Diamonds, ERank::RankK));

	//Hearts
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::RankA));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank2));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank3));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank4));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank5));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank6));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank7));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank8));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank9));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::Rank10));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::RankJ));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::RankQ));
	OrderedDeck.Add(CreatNewCard(ESuit::Hearts, ERank::RankK));

	//Spades
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::RankA));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank2));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank3));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank4));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank5));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank6));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank7));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank8));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank9));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::Rank10));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::RankJ));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::RankQ));
	OrderedDeck.Add(CreatNewCard(ESuit::Spades, ERank::RankK));
}

ACard* ADeck::CreatNewCard(ESuit NewSuit, ERank NewRank)
{
	ACard* NewCard = GetWorld()->SpawnActor<ACard>(ACard::StaticClass());
	NewCard->ValueSuit = NewSuit;
	NewCard->ValueRank = NewRank;
	return NewCard;
}

void ADeck::ShuffleDeckOfCards()
{
	int32 CountCards = OrderedDeck.Num();
	while (CountCards > 0)
	{
		//1. we choose randomly card
		ACard *next = OrderedDeck[rand() % CountCards];
		OrderedDeck.Remove(next);

		//2. and collect in Deck
		AllCards.Add(next);

		CountCards = OrderedDeck.Num();
	}
}

ACard* ADeck::TakeTopCardFromDeck()
{
	ACard* TopCard = AllCards[AllCards.Num() - 1];
	AllCards.Remove(TopCard);
	return TopCard;
}

void ADeck::DealCardsForAll_BeginPlay()
{
	//we take only first 2 cards

	//for Dealer
	for (int i = 0; i < 2; ++i)
	{
		ACard* NewCardD = TakeTopCardFromDeck();
		CardsDealer.Add(NewCardD);
		// AllCards.Remove(NewCardD);
	}
	
	//for Player
	for (int i = 0; i < 2; ++i)
	{
		ACard* NewCardP = TakeTopCardFromDeck();
		CardsPlayer.Add(NewCardP);
		// AllCards.Remove(NewCardP);
	}

	//CalculateScoreOfCards(CardsDealer);
	CurrentScoreOfCardsDealer = CardsDealer[0]->GetValueByRank();
	
	CurrentScoreOfCardsPlayer = CalculateScoreOfCards(CardsPlayer);
}

void ADeck::GetAnAdditionalCard_Dealer()
{		
	while (CurrentScoreOfCardsDealer <= 16)
	{
		ACard* NewCard = TakeTopCardFromDeck();
		CardsDealer.Add(NewCard);
		CurrentScoreOfCardsDealer = CalculateScoreOfCards(CardsDealer);
	}
}

void ADeck::GetAnAdditionalCard_Player()
{
	ACard* NewCard = TakeTopCardFromDeck();
	CardsPlayer.Add(NewCard);
	CurrentScoreOfCardsPlayer = CalculateScoreOfCards(CardsPlayer);
}

int32 ADeck::CalculateScoreOfCards(TArray<ACard*> Cards)
{
	int CurrentScoreOfCards = 0;
	int32 AmountAce = 0; 
	for (auto CurrentCard : Cards)
	{
		int32 ValueCard = CurrentCard->GetValueByRank();	
		CurrentScoreOfCards = CurrentScoreOfCards + ValueCard;

		if (CurrentCard->ValueRank == ERank::RankA)
		{
			AmountAce++;
		}
	}

	if (AmountAce > 0)
	{
		if (CurrentScoreOfCards > 21)
		{
			CurrentScoreOfCards = CurrentScoreOfCards - AmountAce * 10;	
		}
	}

	return CurrentScoreOfCards;
}

bool ADeck::MoreScore(int32 Score)
{
	if (Score > 21)
	{
		return true;
	}
	return false;
}

EResultGame ADeck::CheckWinPlayer()
{
	EResultGame resGame = EResultGame::Draw;
	if (CurrentScoreOfCardsDealer < CurrentScoreOfCardsPlayer)
	{
		resGame = EResultGame::Win;
	}
	else if (CurrentScoreOfCardsDealer > CurrentScoreOfCardsPlayer)
	{
		resGame = EResultGame::Lose;
	}

	return resGame;
}

void ADeck::RestartGame()
{
	CurrentScoreOfCardsDealer = 0;
	CurrentScoreOfCardsPlayer = 0;

	int32 CountCardsDealer = CardsDealer.Num();
	while (CountCardsDealer > 0)
	{
		auto CurrentCardsDealer = CardsDealer[CountCardsDealer-1];
		CardsDealer.Remove(CurrentCardsDealer);
		CountCardsDealer = CardsDealer.Num();
	}

	int32 CountCardsPlayer = CardsPlayer.Num();
	while (CountCardsPlayer > 0)
	{
		auto CurrentCardsPlayer = CardsPlayer[CountCardsPlayer-1];
		CardsPlayer.Remove(CurrentCardsPlayer);
		CountCardsPlayer = CardsPlayer.Num();
	}

	int32 CountAllCards = AllCards.Num();
	while (CountAllCards > 0)
	{
		auto CurrentCard = AllCards[CountAllCards-1];
		AllCards.Remove(CurrentCard);
		CountAllCards = AllCards.Num();
	}
	
	CollectOrderedDeck();
	ShuffleDeckOfCards();
}

// // Called every frame
// void ADeck::Tick(float DeltaTime)
// {
// 	Super::Tick(DeltaTime);
//
// }

