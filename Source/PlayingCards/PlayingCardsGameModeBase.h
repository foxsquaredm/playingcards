// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlayingCardsGameModeBase.generated.h"

UCLASS()
class PLAYINGCARDS_API APlayingCardsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
