// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Card.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "PC_GameInstance.generated.h"

class ACard;

USTRUCT(BlueprintType)
struct FCardInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ACard> Card = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* CardIcon = nullptr;
	
};

UCLASS()
class PLAYINGCARDS_API UPC_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cards")
	UDataTable* DiscCardsTable = nullptr;
	
	UFUNCTION(BlueprintCallable)
	UTexture2D* GetTextureByCard(ACard* Card);
};
