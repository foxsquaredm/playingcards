// Fill out your copyright notice in the Description page of Project Settings.


#include "Card.h"


// Sets default values
ACard::ACard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// // Called when the game starts or when spawned
// void ACard::BeginPlay()
// {
// 	Super::BeginPlay();
// 	
// }
//
// // Called every frame
// void ACard::Tick(float DeltaTime)
// {
// 	Super::Tick(DeltaTime);
//
// }

int32 ACard::GetValueByRank()
{
	int32 result = 0;
	
	switch (ValueRank)
	{
	case ERank::RankA:
	 	result = 11;	// if TotalScore < 21  == 11, else 1
	 	break;
	case ERank::Rank2:
		result = 2;
		break;
	case ERank::Rank3:
		result = 3;
		break;
	case ERank::Rank4:
		result = 4;
		break;
	case ERank::Rank5:
		result = 5;
		break;
	case ERank::Rank6:
		result = 6;
		break;
	case ERank::Rank7:
		result = 7;
		break;
	case ERank::Rank8:
		result = 8;
		break;
	case ERank::Rank9:
		result = 9;
		break;
	case ERank::Rank10:
		result = 10;
		break;
	case ERank::RankJ:
		result = 10;
		break;
	case ERank::RankQ:
		result = 10;
		break;
	case ERank::RankK:
		result = 10;
		break;
	
	default:
		break;
	}
	
	return result;
}
