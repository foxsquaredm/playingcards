// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class ESuit : uint8
{
	Clubs UMETA(DisplayName = "Clubs"),			//Трефы
	Diamonds UMETA(DisplayName = "Diamonds"),	//Бубны
	Hearts UMETA(DisplayName = "Hearts"),		//Червы
	Spades UMETA(DisplayName = "Spades")		//Пики
};

UENUM(BlueprintType)
enum class ERank : uint8
{
	RankA UMETA(DisplayName = "A"),
	Rank2 UMETA(DisplayName = "2"),
	Rank3 UMETA(DisplayName = "3"),
	Rank4 UMETA(DisplayName = "4"),
	Rank5 UMETA(DisplayName = "5"),
	Rank6 UMETA(DisplayName = "6"),
	Rank7 UMETA(DisplayName = "7"),
	Rank8 UMETA(DisplayName = "8"),
	Rank9 UMETA(DisplayName = "9"),
	Rank10 UMETA(DisplayName = "10"),
	RankJ UMETA(DisplayName = "J"),
	RankQ UMETA(DisplayName = "Q"),
	RankK UMETA(DisplayName = "K")
};

UENUM(BlueprintType)
enum class EResultGame : uint8
{
	Win UMETA(DisplayName = "Win"),
	Lose UMETA(DisplayName = "Lose"),
	Draw UMETA(DisplayName = "Draw")
};

class PLAYINGCARDS_API Types
{
public:
	Types();
	~Types();
};
