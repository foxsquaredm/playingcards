// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlayingCards.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PlayingCards, "PlayingCards" );
