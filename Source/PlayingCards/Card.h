// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "GameFramework/Actor.h"
#include "Card.generated.h"

UCLASS()
class PLAYINGCARDS_API ACard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACard();

// protected:
// 	// Called when the game starts or when spawned
// 	virtual void BeginPlay() override;
//
// public:	
// 	// Called every frame
// 	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, Category = "Card")
	ERank ValueRank;
	UPROPERTY(BlueprintReadWrite, Category = "Card")
	ESuit ValueSuit;

	// void SetRankSuit(ERank Rank, ESuit Suit);
	
	UFUNCTION(BlueprintCallable)
	//static int32 GetValue_Card(ERank CurSuit);
	int32 GetValueByRank();
	
};
