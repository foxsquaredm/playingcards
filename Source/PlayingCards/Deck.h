// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Card.h"
#include "GameFramework/Actor.h"
#include "Deck.generated.h"

UCLASS()
class PLAYINGCARDS_API ADeck : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADeck();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BaseMesh;


	////only test
	//UPROPERTY(VisibleAnywhere)
	//TSubclassOf<ACard> PopCard;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
// 	// Called every frame
// 	virtual void Tick(float DeltaTime) override;

	TArray<ACard*> OrderedDeck;

	UPROPERTY(EditAnywhere)
	TArray<ACard*> AllCards;

	UPROPERTY(BlueprintReadWrite)
	TArray<ACard*> CardsDealer;
	UPROPERTY(BlueprintReadWrite)
	TArray<ACard*> CardsPlayer;

	UPROPERTY(BlueprintReadWrite)
	int32 CurrentScoreOfCardsDealer = 0;
	UPROPERTY(BlueprintReadWrite)
	int32 CurrentScoreOfCardsPlayer = 0;
	
	UFUNCTION(BlueprintCallable)
	void CollectOrderedDeck();

	UFUNCTION()
	ACard* CreatNewCard(ESuit NewSuit, ERank NewRank);

	UFUNCTION(BlueprintCallable)
	void ShuffleDeckOfCards();

	UFUNCTION(BlueprintCallable)
	ACard* TakeTopCardFromDeck();

	UFUNCTION(BlueprintCallable)
	void DealCardsForAll_BeginPlay();
	
	UFUNCTION(BlueprintCallable)
	void GetAnAdditionalCard_Dealer();

	UFUNCTION(BlueprintCallable)
	void GetAnAdditionalCard_Player();
	
	UFUNCTION(BlueprintCallable)
	int32 CalculateScoreOfCards(TArray<ACard*> Cards);

	UFUNCTION(BlueprintCallable)
	bool MoreScore(int32 Score);

	UFUNCTION(BlueprintCallable)
	EResultGame CheckWinPlayer();

	UFUNCTION(BlueprintCallable)
	void RestartGame();
};


